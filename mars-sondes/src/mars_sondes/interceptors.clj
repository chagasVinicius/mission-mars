(ns mars-sondes.interceptors
  (:require [mars-sondes.database :as db]))

;; ;; ;; Interceptor
(def sonde-interceptor
  {:name :sonde-interceptor
   :enter
   (fn [context]
     (let [db @db/database
           max-plateu (:max @db/plateu-dim)
           id (keyword (get-in context [:request :path-params :id-sonde]))
           sonde (id db)]
       (update context :request assoc :sonde sonde :max max-plateu)))})

(def all-sondes
  {:name :all-sondes
   :enter
   (fn [context]
     (let [db @db/database]
       (update context :request assoc :all-sondes db)))})

