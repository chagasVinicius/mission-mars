(ns mars-sondes.handlers
  (:require [mars-sondes.core :as core]
            [mars-sondes.database :as db]))

;; Not pure functions
(defn create-sondes! [new-sonde]
  "Creates a new sonde in the database and returns
   the sonde with its respective id"
  (let [id (keyword (core/gen-id))]
    (swap! db/database assoc-in [id] new-sonde)
    {:sonde-id id
     :sonde new-sonde}))

(defn update-plateu-dim! [x y]
  "Updates the max dim (x and y points) of the plateu"
    (let [updated-points {:x x :y y}]
      (swap! db/plateu-dim assoc :max updated-points)
      {:plateu-max-points updated-points}))

;; Handlers
(defn handler-plateu-max!
  [request]
  (let [json-params (get-in request [:json-params])
        [x y] (core/get-vals json-params [:x :y])]
    (if (some nil? [x y])
      (core/error "Arguments not found")
      (let [updated-points (update-plateu-dim! x y)]
        (core/created updated-points)))))

(defn handler-sonde-create!
  [request]
  (let [json-params (get-in request [:json-params])
        [sonde-name x y direction] (core/get-vals json-params [:sonde-name :x :y :direction])]
    (if (or (some nil? [sonde-name x y direction])
            (some empty? [sonde-name x y direction])
            (core/check-direction direction))
      (core/error "Incorrect parameters")
      (let [new-sonde     (core/new-sonde sonde-name x y direction)
            created-sonde (create-sondes! new-sonde)]
        (core/created created-sonde)))))

(defn handler-sonde-commands!
  [request]
  (let [id (keyword (get-in request [:path-params :id-sonde]))
        commands (get-in request [:query-params :commands])
        sonde (get-in request [:sonde])
        max (get-in request [:max])
        [start-coord log-position] (core/get-vals sonde [:coord :log-position])]
    (if (or (some nil? (vals max)) (core/check-commands commands))
      (core/error "Plateu-max not found or incorrect commands inputs")
      (let [new-position (core/update-position start-coord commands max)
            new-log-position (core/update-log-position log-position start-coord new-position)]
        (dosync
         (swap! db/database assoc-in [id :coord] new-position)
         (swap! db/database assoc-in [id :log-position] new-log-position)
         (core/accepted (id @db/database)))))))

(defn handler-all-sondes
  [request]
  (let [all-sondes (get-in request [:all-sondes])]
    (core/ok all-sondes)))
