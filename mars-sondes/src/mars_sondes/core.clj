(ns mars-sondes.core
  (:require [clojure.string :as str]))

;; Variables definition to update-direction
(def ^:private left  {:N :W :W :S :S :E :E :N})
(def ^:private right {:N :E :E :S :S :W :W :N})

(defn response [status body & {:as headers}]
  "Defines the response for a route answer"
  {:status status :body body :headers headers})

(def ok       (partial response 200))
(def created  (partial response 201))
(def accepted (partial response 202))
(def error    (partial response 404))

(defn gen-id [] (gensym "o"))

(defn get-vals [coll keys]
  "Gets multiple values from a collection using multiple keys"
  (map coll keys))

(defn str->keywords [string]
  (map keyword (str/split (str/upper-case string) #"")))

(defn check-commands [string]
  "Checks if the input commands are valid"
  (let [commands (str->keywords string)]
    (some false? (map (fn[x] (contains? (set [:L :M :R]) x)) commands))))

(defn check-direction [string]
  "Checks if the input commands are valid"
  (let [direction (first (str->keywords string))]
    (false? (contains? (set [:N :E :W :S]) direction))))

(defn new-sonde [sonde-name x y direction]
  "Generates a new sonde data structure using an atom."
  {:name sonde-name
   :coord {:x (read-string x)
           :y (read-string y)
           :direction direction}
   :log-position []})


(defn update-log-position [log-position start-coord new-position]
  "Updates log-position with new-position. If the log-position is empty
   It uses the start-coord to conj with the new-position and if the new-postion has some nil coord the sonde signal is losed"
  (cond
    (empty? log-position) (conj (conj log-position start-coord) new-position)
    (some nil? (vals new-position)) (conj log-position ["Sonde signal lost"])
    :else (conj log-position new-position)))

(defn update-y [y direction]
  "Updates y coordinates based in the sonde direction"
  (cond
    (= :N direction) (inc y)
    (= :S direction) (dec y)
    :else y))

(defn update-x [x direction]
  "Updates x coordinates based in the sonde direction"
  (cond
    (= :E direction) (inc x)
    (= :W direction) (dec x)
    :else x))

(defn update-direction [direction turn]
  "Updates direction based in the turn command (L or R)"
  (if (= :L (keyword turn))
    (direction left)
    (direction right)))

(defn update-coords [start-coords command]
  "Updates the sonde's coordinates based in the input commands"
  (let [[x y direction] (get-vals start-coords [:x :y :direction])
        direction (keyword direction)]
    (if (or (= command :L) (= command :R))
      {:x x
       :y y
       :direction (update-direction direction command)}
      {:x (update-x x direction)
       :y (update-y y direction)
       :direction direction})))

(defn test-position-limits [new-x new-y x-max y-max]
  "Tests if the position is inside the plateu limits"
  (or (> new-x (read-string x-max))
      (> new-y (read-string y-max))
      (< new-x 0) (< new-y 0)))

(defn update-position-helper [start-coords commands]
  (let [command (first commands)]
    (if (empty? commands)
      start-coords
      (let [updated-coord (update-coords start-coords command)]
        (conj updated-coord (update-position-helper updated-coord (rest commands)))))))

(defn update-position [start-coord commands max]
  "Updates the sonde's position based on the gived commands"
  (let [[x-max y-max] (get-vals max [:x :y])
        commands (str->keywords commands)
        new-position (update-position-helper start-coord commands)
        [new-x new-y] (get-vals new-position [:x :y])]
    (if (test-position-limits new-x new-y x-max y-max)
      (conj new-position {:x nil :y nil})
      new-position)))
