(ns mars-sondes.service
  (:require [clojure.string :as str]
            [mars-sondes.core :as core]
            [mars-sondes.handlers :as handlers]
            [mars-sondes.database :as db]
            [mars-sondes.interceptors :as interceptors]
            [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            [io.pedestal.http.ring-middlewares :as middlewares]
            [ring.util.response :as ring-resp]))

(def routes
  `[[["/plateu" ^:interceptors [(body-params/body-params)
                                http/json-body]
      ["/plateu-max"
       {:post handlers/handler-plateu-max!}]]

     ["/sondes" ^:interceptors [(body-params/body-params)
                                 http/json-body]

      ["/add-sonde"
       {:post handlers/handler-sonde-create!}]

      ["/commands/:id-sonde" ^:interceptors[interceptors/sonde-interceptor]
       {:post handlers/handler-sonde-commands!}]

      ["/list-sondes" ^:interceptors[interceptors/all-sondes]
       {:get handlers/handler-all-sondes}]]
     ]])

(def service {:env :prod
              ::http/routes routes
              ::http/resource-path "/public"
              ::http/type :jetty
              ::http/port 8080
              ::http/container-options {:h2c? true
                                        :h2? false
                                        :ssl? false}})
