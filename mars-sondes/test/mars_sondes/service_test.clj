(ns mars-sondes.service-test
  (:require [clojure.test :refer :all]
            [io.pedestal.test :refer :all]
            [mars-sondes.core :as core]
            [io.pedestal.http :as bootstrap]
            [cheshire.core :as json]
            [mars-sondes.service :as service]))

(def service
  (::bootstrap/service-fn (bootstrap/create-servlet service/service)))

(deftest get-vals
  (testing "get-vals"
    (let [[x y direction] (core/get-vals {:x 1 :y 2 :direction "N"} [:x :y :direction])]
      (is (= [x y direction] [1 2 "N"])))))

(deftest str->keywords
  (testing "transform string in keywords"
    (let [keyword (core/str->keywords "L")
          keywords (core/str->keywords "LMLMLM")]
      (is (= keyword '(:L)))
      (is (= keywords '(:L :M :L :M :L :M))))))

(deftest check-commands
  (testing "correct commands"
    (let [commands "MLR"
          response (core/check-commands commands)
          expected nil]
      (is (= response expected))))
  (testing "wrong commands"
    (let [commands "ABC"
          response (core/check-commands commands)
          expected true]
      (is (= response expected)))))

(deftest check-commands
  (testing "correct direction N"
    (let [direction "N"
          response (core/check-direction direction)
          expected false]
      (is (= response expected))))
  (testing "correct direction S"
    (let [direction "S"
          response (core/check-direction direction)
          expected false]
      (is (= response expected))))
  (testing "correct direction E"
    (let [direction "N"
          response (core/check-direction direction)
          expected false]
      (is (= response expected))))
  (testing "correct direction W"
    (let [direction "W"
          response (core/check-direction direction)
          expected false]
      (is (= response expected))))
  (testing "incorrect direction A"
    (let [direction "A"
          response (core/check-direction direction)
          expected true]
      (is (= response expected)))))

(deftest new-sonde
  (testing "new-sonde"
    (let [response (core/new-sonde "rover" "1" "2" "N")
          expected-sonde {:name "rover"
                          :coord {:x 1
                                  :y 2
                                  :direction "N"}
                          :log-position []}]
      (is (= response expected-sonde)))))

(deftest update-log-position
  (testing "empty log-position"
    (let [log-position []
          start-coord {:x 1 :y 2 :direction "N"}
          new-position {:x 1 :y 3 :direction "N"}
          response (core/update-log-position log-position start-coord new-position)
          expected [start-coord new-position]]
      (is (= response expected))))
  (testing "some nil val in coord"
    (let [log-position [{:x 1 :y 2 :direction "N"}
                        {:x 1 :y 3 :direction "N"}]
          start-coord {:x 1 :y 3 :direction "N"}
          new-position {:x nil :y nil :direction "N"}
          response (core/update-log-position log-position start-coord new-position)
          expected (conj log-position ["Sonde signal lost"])]
      (is (= response expected))))
  (testing "correct input"
    (let [log-position [{:x 1 :y 2 :direction "N"}
                        {:x 1 :y 3 :direction "N"}]
          start-coord {:x 1 :y 3 :direction "N"}
          new-position {:x 1 :y 4 :direction "N"}
          response (core/update-log-position log-position start-coord new-position)
          expected (conj log-position new-position)]
      (is (= response expected)))))

(deftest update-y
  (testing "N direction"
    (let [[y direction] [1 :N]
          response (core/update-y y direction)
          expected 2]
      (is (= response expected))))
  (testing "S direction"
    (let [[y direction] [1 :S]
          response (core/update-y y direction)
          expected 0]
      (is (= response expected))))
  (testing "E direction"
    (let [[y direction] [1 :E]
          response(core/update-y y direction)
          expected 1]
      (is (= response expected))))
  (testing "W direction"
    (let [[y direction] [1 :W]
          response(core/update-y y direction)
          expected 1]
      (is (= response expected)))))

(deftest update-x
  (testing "E direction"
    (let [[x direction] [1 :E]
          response (core/update-x x direction)
          expected 2]
      (is (= response expected))))
  (testing "W direction"
    (let [[x direction] [1 :W]
          response (core/update-x x direction)
          expected 0]
      (is (= response expected))))
  (testing "N direction"
    (let [[x direction] [1 :N]
          response(core/update-x x direction)
          expected 1]
      (is (= response expected))))
  (testing "S direction"
    (let [[x direction] [1 :S]
          response(core/update-x x direction)
          expected 1]
      (is (= response expected)))))

(deftest update-direction
  (testing "Turn left. Starting in N"
    (let [[direction turn] [:N :L]
          response (core/update-direction direction turn)
          expected :W]
      (is (= response expected))))
  (testing "Turn left. Starting in W"
    (let [[direction turn] [:W :L]
          response (core/update-direction direction turn)
          expected :S]
      (is (= response expected))))
  (testing "Turn left. Starting in S"
    (let [[direction turn] [:S :L]
          response (core/update-direction direction turn)
          expected :E]
      (is (= response expected))))
  (testing "Turn left. Starting in E"
    (let [[direction turn] [:E :L]
          response (core/update-direction direction turn)
          expected :N]
      (is (= response expected))))
  (testing "Turn right. Starting in N"
    (let [[direction turn] [:N :R]
          response (core/update-direction direction turn)
          expected :E]
      (is (= response expected))))
  (testing "Turn right. Starting in E"
    (let [[direction turn] [:E :R]
          response (core/update-direction direction turn)
          expected :S]
      (is (= response expected))))
  (testing "Turn right. Starting in S"
    (let [[direction turn] [:S :R]
          response (core/update-direction direction turn)
          expected :W]
      (is (= response expected))))
  (testing "Turn right. Starting in W"
    (let [[direction turn] [:W :R]
          response (core/update-direction direction turn)
          expected :N]
      (is (= response expected)))))

(deftest test-position-limits
  (testing "Inside limits"
    (let [[new-x new-y x-max y-max] [4 4 "5" "5"]
          response (core/test-position-limits new-x new-y x-max y-max)
          expected false]
      (is (= response expected))))
  (testing "Outside limits. > max"
    (let [[new-x new-y x-max y-max] [6 6 "5" "5"]
          response (core/test-position-limits new-x new-y x-max y-max)
          expected true]
      (is (= response expected))))
  (testing "Outside limits. < 0"
    (let [[new-x new-y x-max y-max] [-1 -1 "5" "5"]
          response (core/test-position-limits new-x new-y x-max y-max)
          expected true]
      (is (= response expected)))))

(deftest update-coords
  (testing "Turn commands. L, starting in N"
    (let [[start-coords command] [{:x 1 :y 2 :direction "N"} :L]
          response (core/update-coords start-coords command)
          expected {:x 1 :y 2 :direction :W}]
      (is (= response expected))))
  (testing "Turn commands. R, starting in N"
    (let [[start-coords command] [{:x 1 :y 2 :direction "N"} :R]
          response (core/update-coords start-coords command)
          expected {:x 1 :y 2 :direction :E}]
      (is (= response expected))))
  (testing "Turn commands. M, starting in N"
    (let [[start-coords command] [{:x 1 :y 2 :direction "N"} :M]
          response (core/update-coords start-coords command)
          expected {:x 1 :y 3 :direction :N}]
      (is (= response expected))))
  (testing "Turn commands. M, starting in S"
    (let [[start-coords command] [{:x 1 :y 2 :direction "S"} :M]
          response (core/update-coords start-coords command)
          expected {:x 1 :y 1 :direction :S}]
      (is (= response expected))))
  (testing "Turn commands. M, starting in W"
    (let [[start-coords command] [{:x 1 :y 2 :direction "W"} :M]
          response (core/update-coords start-coords command)
          expected {:x 0 :y 2 :direction :W}]
      (is (= response expected))))
  (testing "Turn commands. M, starting in E"
    (let [[start-coords command] [{:x 1 :y 2 :direction "E"} :M]
          response (core/update-coords start-coords command)
          expected {:x 2 :y 2 :direction :E}]
      (is (= response expected)))))

(deftest update-position
  (testing "With new-postion inside the max limits"
    (let [[start-coord max] [{:x 1 :y 2 :direction "N"} {:x "5" :y "5"}]
          commands "MMM"
          response (core/update-position start-coord commands max)
          expected {:x 1 :y 5 :direction :N}]
      (is (= response expected)))))

