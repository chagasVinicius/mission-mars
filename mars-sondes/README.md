# mars-sondes

Este e o projeto mars-sondes, um RESTful web service que controla os movimentos de uma sonda na exploracao de marte. O servico possui a arquitetura basica de uma RESTful web application com os seguintes verbos:

- POST Os pontos maximos do planalto
```
POST "/plateu/plateu-max" [x y]
```
- POST Adiciona uma sonda
```
POST "/sondes/add-sonde" [sonde-name x y direction]
```
- POST Os comandos de movimentos
```
POST "/sondes/:id-sonde?comands="
```
- GET Todas as sondas disponíveis
```
GET "/sondes/list-sondes"
```

## Pre-requisitos

Para seguir o **tutorial** voce precisa:

1. Realizar download do codigo deste repositorio.
2. Clojure 1.8.0 ou acima instalado. [Clojure] (https://clojure.org/index)
3. Leiningen 2.0.0 ou acima instalado . [leiningen] (https://leiningen.org/)
4. HTTPie um clinete HTTP por linha de comando instalado. [HTTPie] (https://httpie.org/doc#usage)

## Tutorial

Depois de realizar os downloads de pre-requisito, dentro da pasta mars-sondes, voce pode iniciar o wev service utilizando:

- O comando **lein run**
```
lein run
```

Em outro terminal, realize os comandos de requisicoes utilizando o HTTPie.

Insira os pontos maximos do planalto:

```
http POST :8080/plateu/plateu-max x=5 y=5
```

Insira a pirmeira sonda e sua posicao inicial

```
http POST :8080/sondes/add-sonde sonde-name=Rover x=1 y=2 direction=N
```

O retorno do comando anterior ira retornar qual o id da sonda adicionada
(i.e)

```
{
    "sonde": {
        "coord": {
            "direction": "N",
            "x": 1,
            "y": 2
        },
        "log-position": [],
        "name": "Rover"
    },
    "sonde-id": "o13227" <<< id-sonde
}

```
Insira os comandos para os movimentos da sonda utilizando seu id

```
http POST :8080/sondes/commands/o13227?commands=LMLMLMLMM
```
O retorno da ***request*** possuira as coordenadas da sonda apos os comandos, assim como seu log de coordenadas. 

```
{
    "coord": {                <<<< novas coordenads
        "direction": "N",
        "x": 1,
        "y": 3
    },
    "log-position": [         <<<< log-coordenadas
        {
            "direction": "N",
            "x": 1,
            "y": 2
        },
        {
            "direction": "N",
            "x": 1,
            "y": 3
        }
    ],
    "name": "Rover"
}
```
Repetindo os comandos anteriores para adicionar uma nova sonda

```
http POST :8080/sondes/add-sonde sonde-name=Mars x=3 y=3 direction=E
http POST :8080/sondes/commands/o13228?commands=MMRMMRMRRM
```
Por ultimo, podemos resgatar a informacao de todas as sondas disponiveis 

```
http GET :8080/sondes/list-sondes
```

### Testes
1. Rode os testes do servico: `lein test`

### [Docker](https://www.docker.com/) container support

1. Crie um uberjar do servico: `lein uberjar`
2. Crie uma docker image: `sudo docker build -t mars-sondes .`
3. Inicialize o  Docker image: `docker run -p 8080:8080 mars-sondes`


